Trajanje izvrsavanja:

1. ucitajPodatke -> 8669 ms
2. pretraziZapise -> 566 ms
3. obrisiZapise -> 846 ms
4. dohvatiNajvecuVrijednost -> 12315 ms
5. dohvatiNajmanjuVrijednost -> 6190 ms
6. dodajZapis -> 350ms


ucitajPodatke:
	prvo se otvara datoteka koju smo prosljedili funkciji ako nije javit će grešku inače prelazi na čitanje datoteke.
	Čitanje datoteke se odvija da se prvo preskače prva linija jer to su zapravo samo nazivi stupaca, ali za ostale retke prolazi kroz petlju i
	svaki red sprema u string. No da znamo koji je podatak odgovara kojem stupcu potrebmo je bilo razdvojiti redke na ćelije kako bi se ti podaci prepoznali.
	(delimiter je zarez i kada naleti na zarez on taj poadtak zatim vidi kao ćeliju). Zatim se te ćelije dodaju u unordered_map nazvanu redak. Ključ ovdje predstavlja
	STUPAC a vrijednost je sadržaj unutar ćelije. Push_back-amo "redak" ali se time svaki redak dodaje u vektor "podaci".

pretraziZapise:
	Parametri koji se primaju u ovoj funkciji su KEY(stupac), VALUE(vrijednost pod tim stupcem) i N (broj podataka za pretragu/limit).
	Problem je što su nam neki podaci drukčijeg tipa odnosno nisu svi string i nisu svi int ili float itd. Stoga je potrebno napraviti konverziju.
	Konverzija se radi tako da prvo inicijaliziramo sve tipove podataka (za stupce), te se onda ako je string ostavlja po default u stringu a ako
	nije onda se radi pretvorba u onaj tip koji je inicijaliziran za taj stupac. Za pronalazak koliko smo našli pronađenih nam je potreban jer moramo
	ograničiti pretragu na N vrijednsti. "std::string tip = tipoviPodataka[key];" preuzima tip podatka za stupac te zatim mapa tipoviPodataka povezuje
	ključeve stupaca kako smo inicijalizirali i tipove podataka. Prva for petlja prolazi kroz vektor podaci, zatim se traži ključ u tom retku te ako je
	pronađen onda se uspoređuju (traže) vrijednosti. Ako je zapis pronađen, ispisuju se svi podaci iz tog retka te se brojač povećeva.

obrisiZapise:
	Isto kao i kod pretrage pronaći će se vrijednosti za brisanje. Kada su pronađene u "if (it->find(key) != it->end())" onda se one brišu ali tako da se
	prebacuje na sljedeci podatak nakon brisanja. Te kao i kod pretraziZapise brojač vodi brigu o tome da se ne pređe limit N ondosno koliko smo rekli da se obrise zapisa.

dohvatiNajvecuVrijednost:
	Kreira se "multimap<double, std::unordered_map<int, std::string>, std::greater<>> sorted;" to je multimapa koja će automatski sortirati svoje elemente prema ključu u silaznom redoslijedu
	preko "std::greater<>".
	Bitno je reći da se zatim i ti podaci (ključevi) koji su pronađeni u "it != redak.end()" dodaju u "sorted" multimapu.
	Ispisivanje je isto kao i do sada imamo broječ koji broji koliko je zapisa prikazano i time ograničava koliko smo podataka htjeli. Naravno da bi dobili te podatke trebamo ponovno proci kroz
	sve storitane elemente unutar "sorted".

dohvatiNajmanjuVrijednost:
	Isto kao i kod prijašnje funkcije samo se u multimapu automatski sotritaju elementi prema ključu u uzlaznom redoslijedu.

dodajZapis:
	Svaki zapis je predstavljen kao "unordered_map<int, std::string>", gdje je ključ stupac i vrijednost je string (sadržaj ćelije u retku). For petljom prolazimo kroz svaki element unutar vektora noviZapisi
	i sprema ga sa ostalim podacima.

U ovom projektu koristio sam strukture podataka kao što su vektori, unordered_map i multimap. 
Vektori su korišteni zbog njihove sposobnosti dinamičkog pohranjivanja i jednostavnog pristupa podacima. 
Unordered_mape su bile korisne za mapiranje ključeva sa vrijednostima, što omogućava efikasnu organizaciju i pretragu podataka. 
Multimape su posebno korisne jer automatski sortiraju podatke, što olakšava i čini efikasnijim postupke poput dohvaćanja podataka s najvećim ili najmanjim vrijednostima.