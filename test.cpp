#include <iostream>
#include <fstream>
#include <sstream>

#include <algorithm>
#include <vector>
#include <unordered_map>
#include <map>
#include <string>

#include <chrono>

class BAZA
{
public:
    BAZA()
    {
        inicijalizirajTipovePodataka();
    }

    void ucitajPodatke(const std::string &filename)
    {
        std::ifstream file(filename);
        std::string line, cell;

        if (!file.is_open())
        {
            std::cerr << "Ne mogu otvoriti datoteku: " << filename << std::endl;
            return;
        }

        std::getline(file, line);

        while (std::getline(file, line))
        {
            std::istringstream linestream(line);
            std::unordered_map<int, std::string> redak;
            int stupacIndex = 0;

            while (std::getline(linestream, cell, ','))
            {
                redak[stupacIndex++] = cell;
            }

            podaci.push_back(redak);
        }

        file.close();
    }

    void pretraziZapise(int key, const std::string &value, int n)
    {
        int brojPronadjenih = 0;
        std::string tip = tipoviPodataka[key];
        double numerickaVrijednost = konvertirajUVrijednost(value, tip);

        for (const auto &redak : podaci)
        {
            auto it = redak.find(key);
            if (it != redak.end())
            {
                double vrijednostURedku = konvertirajUVrijednost(it->second, tip);
                if ((tip == "string" && it->second == value) || (tip != "string" && vrijednostURedku == numerickaVrijednost))
                {
                    for (const auto &par : redak)
                    {
                        std::cout << par.first << ": " << par.second << ", ";
                    }
                    std::cout << std::endl;
                    brojPronadjenih++;

                    if (n > 0 && brojPronadjenih >= n)
                    {
                        break;
                    }
                }
            }
        }
        if (brojPronadjenih == 0)
        {
            std::cout << "Nema zapisa koji zadovoljavaju kriterije pretrage." << std::endl;
        }
    }

    void obrisiZapise(int key, const std::string &value, int n)
    {
        int brojObrisanih = 0;
        std::string tip = tipoviPodataka[key];
        double numerickaVrijednost = konvertirajUVrijednost(value, tip);

        for (auto it = podaci.begin(); it != podaci.end();)
        {
            if (it->find(key) != it->end())
            {
                double vrijednostURedku = konvertirajUVrijednost(it->at(key), tip);
                if ((tip == "string" && it->at(key) == value) || (tip != "string" && vrijednostURedku == numerickaVrijednost))
                {
                    it = podaci.erase(it);
                    brojObrisanih++;

                    if (n > 0 && brojObrisanih >= n)
                    {
                        break;
                    }
                }
                else
                {
                    ++it;
                }
            }
            else
            {
                ++it;
            }
        }

        if (brojObrisanih == 0)
        {
            std::cout << "Nema zapisa koji zadovoljavaju kriterije za brisanje." << std::endl;
        }
        else
        {
            std::cout << "Obrisano " << brojObrisanih << " zapisa." << std::endl;
        }
    }

    void dohvatiNajvecuVrijednost(int key, int n)
    {
        if (podaci.empty())
        {
            std::cout << "Baza podataka je prazna." << std::endl;
            return;
        }

        std::string tip = tipoviPodataka[key];
        std::multimap<double, std::unordered_map<int, std::string>, std::greater<>> sorted;

        for (const auto &redak : podaci)
        {
            auto it = redak.find(key);
            if (it != redak.end())
            {
                double vrijednost = konvertirajUVrijednost(it->second, tip);
                sorted.insert({vrijednost, redak});
            }
        }

        int brojPrikazanih = 0;
        for (const auto &item : sorted)
        {
            if (n > 0 && brojPrikazanih >= n)
            {
                break;
            }
            for (const auto &par : item.second)
            {
                std::cout << par.first << ": " << par.second << ", ";
            }
            std::cout << std::endl;
            brojPrikazanih++;
        }

        if (brojPrikazanih == 0)
        {
            std::cout << "Nema zapisa koji zadovoljavaju kriterije." << std::endl;
        }
    }

    void dohvatiNajmanjuVrijednost(int key, int n)
    {
        if (podaci.empty())
        {
            std::cout << "Baza podataka je prazna." << std::endl;
            return;
        }

        std::string tip = tipoviPodataka[key];
        std::multimap<double, std::unordered_map<int, std::string>> sorted;

        for (const auto &redak : podaci)
        {
            auto it = redak.find(key);
            if (it != redak.end())
            {
                double vrijednost = konvertirajUVrijednost(it->second, tip);
                sorted.insert({vrijednost, redak});
            }
        }

        int brojPrikazanih = 0;
        for (const auto &item : sorted)
        {
            if (n > 0 && brojPrikazanih >= n)
            {
                break;
            }
            for (const auto &par : item.second)
            {
                std::cout << par.first << ": " << par.second << ", ";
            }
            std::cout << std::endl;
            brojPrikazanih++;
        }

        if (brojPrikazanih == 0)
        {
            std::cout << "Nema zapisa koji zadovoljavaju kriterije." << std::endl;
        }
    }

    void dodajZapis(const std::vector<std::unordered_map<int, std::string>> &noviZapisi)
    {
        for (const auto &zapis : noviZapisi)
        {
            podaci.push_back(zapis);
        }

        std::cout << "Dodano " << noviZapisi.size() << " novih zapisa." << std::endl;
    }

private:
    std::vector<std::unordered_map<int, std::string>> podaci;
    std::unordered_map<int, std::string> tipoviPodataka;

    void inicijalizirajTipovePodataka()
    {
        // Numerički stupci
        tipoviPodataka[1] = "int";   // year
        tipoviPodataka[2] = "int";   // day
        tipoviPodataka[3] = "int";   // length
        tipoviPodataka[4] = "float"; // weight
        tipoviPodataka[5] = "int";   // count
        tipoviPodataka[6] = "int";   // looped
        tipoviPodataka[7] = "int";   // neighbors
        tipoviPodataka[8] = "float"; // income

        // Stupci sa stringovima
        tipoviPodataka[0] = "string"; // address
        tipoviPodataka[9] = "string"; // label
    }

    double konvertirajUVrijednost(const std::string &value, const std::string &tip)
    {
        try
        {
            if (tip == "int")
            {
                return std::stoi(value);
            }
            else if (tip == "float")
            {
                return std::stof(value);
            }
            else
            {
                // Ako nije numerički tip
                return 0;
            }
        }
        catch (const std::invalid_argument &e)
        {
            std::cerr << "Greška u konverziji: " << e.what() << std::endl;
            return 0;
        }
    }
};

int main()
{
    BAZA baza;

    std::cout << "Ucitavanje podataka iz 'BitcoinHeistData.csv'..." << std::endl;
    baza.ucitajPodatke("BitcoinHeistData.csv");

    std::cout << "\nPretrazivanje zapisa za stupac 'year' (1) s vrijednosti '2016'..." << std::endl;
    baza.pretraziZapise(1, "0", 1);

    std::cout << "\nBrisanje zapisa za stupac 'year' (1) s vrijednosti '2015'..." << std::endl;
    baza.obrisiZapise(1, "2015", 3);

    std::cout << "\nPretrazivanje zapisa za stupac 'year' (1) s vrijednosti '2015' nakon brisanja..." << std::endl;
    baza.pretraziZapise(1, "2015", 1);

    std::cout << "\nDohvacanje zapisa s najvecom vrijednosti za stupac 'income' (8)..." << std::endl;
    baza.dohvatiNajvecuVrijednost(8, 1);

    std::cout << "\nDohvacanje zapisa s najmanjom vrijednosti za stupac 'day' (2)..." << std::endl;
    baza.dohvatiNajmanjuVrijednost(2, 1);

    std::cout << "\nDodavanje novih zapisa..." << std::endl;
    std::vector<std::unordered_map<int, std::string>> noviZapisi;
    std::unordered_map<int, std::string> zapis1 = {{0, "novaAdresa1"}, {1, "2019"}, {2, "100"}, {3, "50"}, {4, "0.5"}, {5, "2"}, {6, "1"}, {7, "3"}, {8, "50000"}, {9, "label1"}};
    noviZapisi.push_back(zapis1);
    baza.dodajZapis(noviZapisi);

    std::cout << "\nPretrazivanje novododanog zapisa..." << std::endl;
    baza.pretraziZapise(0, "novaAdresa1", 1);

    return 0;
}